// Copyright 2020 IOTA Stiftung
// SPDX-License-Identifier: Apache-2.0

// (Re-)generated by schema tool
// >>>> DO NOT CHANGE THIS FILE! <<<<
// Change the json schema instead

package langurburja

import "github.com/iotaledger/wasp/packages/vm/wasmlib/go/wasmlib"

type ImmutableLastWinningDrawResults struct {
	id int32
}

func (s ImmutableLastWinningDrawResults) LastWinningDraw() ImmutableDraw {
	return ImmutableDraw{objID: s.id, keyID: idxMap[IdxResultLastWinningDraw]}
}

type MutableLastWinningDrawResults struct {
	id int32
}

func (s MutableLastWinningDrawResults) LastWinningDraw() MutableDraw {
	return MutableDraw{objID: s.id, keyID: idxMap[IdxResultLastWinningDraw]}
}

type ImmutableRoundNumberResults struct {
	id int32
}

func (s ImmutableRoundNumberResults) RoundNumber() wasmlib.ScImmutableInt64 {
	return wasmlib.NewScImmutableInt64(s.id, idxMap[IdxResultRoundNumber])
}

type MutableRoundNumberResults struct {
	id int32
}

func (s MutableRoundNumberResults) RoundNumber() wasmlib.ScMutableInt64 {
	return wasmlib.NewScMutableInt64(s.id, idxMap[IdxResultRoundNumber])
}

type ImmutableRoundStartedAtResults struct {
	id int32
}

func (s ImmutableRoundStartedAtResults) RoundStartedAt() wasmlib.ScImmutableInt32 {
	return wasmlib.NewScImmutableInt32(s.id, idxMap[IdxResultRoundStartedAt])
}

type MutableRoundStartedAtResults struct {
	id int32
}

func (s MutableRoundStartedAtResults) RoundStartedAt() wasmlib.ScMutableInt32 {
	return wasmlib.NewScMutableInt32(s.id, idxMap[IdxResultRoundStartedAt])
}

type ImmutableRoundStatusResults struct {
	id int32
}

func (s ImmutableRoundStatusResults) RoundStatus() wasmlib.ScImmutableInt8 {
	return wasmlib.NewScImmutableInt8(s.id, idxMap[IdxResultRoundStatus])
}

type MutableRoundStatusResults struct {
	id int32
}

func (s MutableRoundStatusResults) RoundStatus() wasmlib.ScMutableInt8 {
	return wasmlib.NewScMutableInt8(s.id, idxMap[IdxResultRoundStatus])
}
