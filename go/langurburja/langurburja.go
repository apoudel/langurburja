// Copyright 2021 Aayush Poudel
// SPDX-License-Identifier: Apache-2.0

package langurburja

import (
    "github.com/iotaledger/wasp/packages/vm/wasmlib/go/wasmlib"
    "strconv"
)

const EnableSelfPost = true
const NanoTimeDivider = 1000_000_000
const DefaultPlayPeriod = 60
const MaxSymbolId = 5

func funcForceReset(ctx wasmlib.ScFuncContext, f *ForceResetContext ) {
	f.State.Bets().Clear()
	f.State.RoundStatus().SetValue(0)
	f.Events.Stop()
}

func funcInit(ctx wasmlib.ScFuncContext, f *InitContext) {
    if f.Params.Owner().Exists() {
        f.State.Owner().SetValue( f.Params.Owner().Value() )
        return
    }
    f.State.Owner().SetValue( ctx.ContractCreator() )

    draw := & Draw { 0,1,0,2,0,3 }
	f.State.LastWinningDraw().SetValue( draw )
}

func funcPayOwner(ctx wasmlib.ScFuncContext, f *PayOwnerContext) {
    var balances wasmlib.ScBalances = ctx.Balances()
    var iotaBalance int64 = balances.Balance( wasmlib.IOTA )

    ctx.Log( "IOTA balance " + strconv.FormatInt( iotaBalance, 10 ))

	amount := f.Params.Amount().Value()

    if amount > 0 {
	    transfers := wasmlib.NewScTransferIotas( amount )
	    ctx.TransferToAddress( f.State.Owner().Value().Address(), transfers )
    }
}

func funcPayWinners(ctx wasmlib.ScFuncContext, f *PayWinnersContext) {
    // map if symbolId -> symbolCount
    symbolMap := map[int8]int8  {
        0: 0, // club  count
        1: 0, // flag  count
        2: 0, // spade count
        3: 0, // heart count
        4: 0, // brick count
        5: 0, // crown count
    }

    winningDraw := & Draw {
		0, 0, 0, 0, 0, 0,
    }

    for i := 0; i <= MaxSymbolId ; i++ {
	    symbolId := int8( ctx.Random( MaxSymbolId + 1 ))
        symbolMap[ symbolId ] += 1

        if i == 0 {
			winningDraw.Dice1 = symbolId;
		} else if i == 1 {
			winningDraw.Dice2 = symbolId;
		} else if i == 2 {
			winningDraw.Dice3 = symbolId;
		} else if i == 3 {
			winningDraw.Dice4 = symbolId;
		} else if i == 4 {
			winningDraw.Dice5 = symbolId;
		} else if i == 5 {
			winningDraw.Dice6 = symbolId;
		}
    }

	f.State.LastWinningDraw().SetValue( winningDraw )

	f.Events.Winner( winningDraw.Dice1, winningDraw.Dice2, winningDraw.Dice3, winningDraw.Dice4, winningDraw.Dice5, winningDraw.Dice6 )

	bets := f.State.Bets()
	nrBets := bets.Length()

	for i := int32(0); i < nrBets; i++ {
		// Get the next winner.
		bet := bets.GetBet(i).Value()

        symbolCount := symbolMap[ bet.SymbolId ]

		if symbolCount >= 2 {
            payout := bet.Amount * int64( symbolCount )

			transfers := wasmlib.NewScTransferIotas( payout )
			ctx.TransferToAddress( bet.Better.Address(), transfers )

		    f.Events.Payout( bet.Better.Address(), payout )
        }
	}

	bets.Clear()

	// Set round status to 0, send out event to notify that the round has ended
	f.State.RoundStatus().SetValue(0)
	f.Events.Stop()
}

func funcPlaceBet(ctx wasmlib.ScFuncContext, f *PlaceBetContext) {
	symbolId := f.Params.SymbolId().Value()
	bets := f.State.Bets()

	for i := int32(0); i < bets.Length(); i++ {
		bet := bets.GetBet(i).Value()

		if bet.Better.Address() == ctx.Caller().Address() && bet.SymbolId == symbolId {
			ctx.Panic("Bet already placed for this round")
		}
	}

	// Require that the number is a valid number to bet on, otherwise panic out.
	ctx.Require( symbolId >= 0 && symbolId <= MaxSymbolId, "invalid symbolId")

	// Create ScBalances proxy to the incoming balances for this request.
	// Note that ScBalances wraps an ScImmutableMap of token color/amount combinations
	// in a simpler to use interface.
	incoming := ctx.Incoming()

	// Retrieve the amount of plain iota tokens that are part of the incoming balance.
	amount := incoming.Balance( wasmlib.IOTA )

	// Require that there are actually some plain iotas there
	ctx.Require( amount > 0, "empty bet")

	// Now we gather all information together into a single serializable struct
	// Note that we use the caller() method of the function context to determine
	// the agent id of the better. This is where a potential pay-out will be sent.
	bet := &Bet{
		Better: ctx.Caller(),
		Amount: amount,
		SymbolId: symbolId,
	}

	// Determine what the next bet number is by retrieving the length of the bets array.
	betNr := bets.Length()

	// Append the bet data to the bets array. The bet array will automatically take care
	// of serializing the bet struct into a bytes representation.
	bets.GetBet( betNr ).SetValue( bet )

	f.Events.Bet( bet.Better.Address(), bet.Amount, bet.SymbolId )

	// Was this the first bet of this round?
	if betNr == 0 {
		// Yes it was, query the state for the length of the playing period in seconds by
		// retrieving the playPeriod value from state storage
		playPeriod := f.State.PlayPeriod().Value()

		// if the play period is less than 10 seconds we override it with the default duration.
		// Note that this will also happen when the play period was not set yet because in that
		// case a zero value was returned.
		if playPeriod < 10 {
			playPeriod = DefaultPlayPeriod
            f.State.PlayPeriod().SetValue( playPeriod );
		}

		if EnableSelfPost {
			f.State.RoundStatus().SetValue(1)

			// timestamp is nanotime, divide by NanoTimeDivider to get seconds => common unix timestamp
			timestamp := int32( ctx.Timestamp() / NanoTimeDivider)
			f.State.RoundStartedAt().SetValue( timestamp )

			f.Events.Start()

			roundNumber := f.State.RoundNumber()
			roundNumber.SetValue( roundNumber.Value() + 1)

			f.Events.Round( roundNumber.Value())

			// And now for our next trick we post a delayed request to ourselves on the Tangle.
			// We are requesting to call the 'payWinners' function, but delay it for the playPeriod
			// amount of seconds. This will lock in the playing period, during which more bets can
			// be placed. Once the 'payWinners' function gets triggered by the ISCP it will gather
			// all bets up to that moment as the ones to consider for determining the winner.
			ScFuncs.PayWinners( ctx ).Func.Delay( playPeriod ).TransferIotas(1).Post()
		}
	}
}

func funcPlayPeriod(ctx wasmlib.ScFuncContext, f *PlayPeriodContext) {
	playPeriod := f.Params.PlayPeriod().Value()
	ctx.Require( playPeriod >= 10, "invalid play period")
	f.State.PlayPeriod().SetValue( playPeriod )
}

func funcSetOwner(ctx wasmlib.ScFuncContext, f *SetOwnerContext) {
	f.State.Owner().SetValue( f.Params.Owner().Value())
}

func viewLastWinningDraw(ctx wasmlib.ScViewContext, f *LastWinningDrawContext) {
    lastWinningDraw := f.State.LastWinningDraw().Value()
	f.Results.LastWinningDraw().SetValue( lastWinningDraw )
}

func viewRoundNumber(ctx wasmlib.ScViewContext, f *RoundNumberContext) {
	roundNumber := f.State.RoundNumber().Value()
	f.Results.RoundNumber().SetValue( roundNumber )
}

func viewRoundStartedAt(ctx wasmlib.ScViewContext, f *RoundStartedAtContext) {
	roundStartedAt := f.State.RoundStartedAt().Value()
	f.Results.RoundStartedAt().SetValue( roundStartedAt)
}

func viewRoundStatus(ctx wasmlib.ScViewContext, f *RoundStatusContext) {
	roundStatus := f.State.RoundStatus().Value()
	f.Results.RoundStatus().SetValue( roundStatus )
}
