// Copyright 2020 IOTA Stiftung
// SPDX-License-Identifier: Apache-2.0

package test

import (
	"testing"

	"../go/langurburja"
	"github.com/iotaledger/wasp/packages/vm/wasmsolo"
	"github.com/stretchr/testify/require"
)

var seed = ed25519.NewSeed([]byte("long long seed for determinism................"))

func TestDeploy(t *testing.T) {
    env := solo.New(t, false, false, seed)
    chain := env.NewChain(nil, "KSC")

    // calls view root::GetChainInfo
    chainID, chainOwnerID, coreContracts := chain.GetInfo()
    // assert all core contracts deployed (default)
    require.EqualValues(t, len(core.AllCoreContractsByHash), len(coreContracts))

    t.Logf("chain ID: %s", chainID.String())
    t.Logf("chain owner ID: %s", chainOwnerID.String())
    for hname, rec := range coreContracts {
        t.Logf("    Core contract '%s': %s", rec.Name, iscp.NewAgentID(chainID.AsAddress(), hname))
    }
}
