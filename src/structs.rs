// Copyright 2020 IOTA Stiftung
// SPDX-License-Identifier: Apache-2.0

// (Re-)generated by schema tool
// >>>> DO NOT CHANGE THIS FILE! <<<<
// Change the json schema instead

#![allow(dead_code)]
#![allow(unused_imports)]

use wasmlib::*;
use wasmlib::host::*;

pub struct Bet {
    pub amount    : i64, 
    pub better    : ScAgentID, 
    pub symbol_id : i8,  // Player will choose among 6 symbols. club 0, flag 1, spade 2, heart 3, brick 4, crown 5
}

impl Bet {
    pub fn from_bytes(bytes: &[u8]) -> Bet {
        let mut decode = BytesDecoder::new(bytes);
        Bet {
            amount    : decode.int64(),
            better    : decode.agent_id(),
            symbol_id : decode.int8(),
        }
    }

    pub fn to_bytes(&self) -> Vec<u8> {
        let mut encode = BytesEncoder::new();
		encode.int64(self.amount);
		encode.agent_id(&self.better);
		encode.int8(self.symbol_id);
        return encode.data();
    }
}

pub struct ImmutableBet {
    pub(crate) obj_id: i32,
    pub(crate) key_id: Key32,
}

impl ImmutableBet {
    pub fn exists(&self) -> bool {
        exists(self.obj_id, self.key_id, TYPE_BYTES)
    }

    pub fn value(&self) -> Bet {
        Bet::from_bytes(&get_bytes(self.obj_id, self.key_id, TYPE_BYTES))
    }
}

pub struct MutableBet {
    pub(crate) obj_id: i32,
    pub(crate) key_id: Key32,
}

impl MutableBet {
    pub fn delete(&self) {
        del_key(self.obj_id, self.key_id, TYPE_BYTES);
    }

    pub fn exists(&self) -> bool {
        exists(self.obj_id, self.key_id, TYPE_BYTES)
    }

    pub fn set_value(&self, value: &Bet) {
        set_bytes(self.obj_id, self.key_id, TYPE_BYTES, &value.to_bytes());
    }

    pub fn value(&self) -> Bet {
        Bet::from_bytes(&get_bytes(self.obj_id, self.key_id, TYPE_BYTES))
    }
}

pub struct Draw {
    pub dice1 : i8,  // symbolId on each dice
    pub dice2 : i8, 
    pub dice3 : i8, 
    pub dice4 : i8, 
    pub dice5 : i8, 
    pub dice6 : i8, 
}

impl Draw {
    pub fn from_bytes(bytes: &[u8]) -> Draw {
        let mut decode = BytesDecoder::new(bytes);
        Draw {
            dice1 : decode.int8(),
            dice2 : decode.int8(),
            dice3 : decode.int8(),
            dice4 : decode.int8(),
            dice5 : decode.int8(),
            dice6 : decode.int8(),
        }
    }

    pub fn to_bytes(&self) -> Vec<u8> {
        let mut encode = BytesEncoder::new();
		encode.int8(self.dice1);
		encode.int8(self.dice2);
		encode.int8(self.dice3);
		encode.int8(self.dice4);
		encode.int8(self.dice5);
		encode.int8(self.dice6);
        return encode.data();
    }
}

pub struct ImmutableDraw {
    pub(crate) obj_id: i32,
    pub(crate) key_id: Key32,
}

impl ImmutableDraw {
    pub fn exists(&self) -> bool {
        exists(self.obj_id, self.key_id, TYPE_BYTES)
    }

    pub fn value(&self) -> Draw {
        Draw::from_bytes(&get_bytes(self.obj_id, self.key_id, TYPE_BYTES))
    }
}

pub struct MutableDraw {
    pub(crate) obj_id: i32,
    pub(crate) key_id: Key32,
}

impl MutableDraw {
    pub fn delete(&self) {
        del_key(self.obj_id, self.key_id, TYPE_BYTES);
    }

    pub fn exists(&self) -> bool {
        exists(self.obj_id, self.key_id, TYPE_BYTES)
    }

    pub fn set_value(&self, value: &Draw) {
        set_bytes(self.obj_id, self.key_id, TYPE_BYTES, &value.to_bytes());
    }

    pub fn value(&self) -> Draw {
        Draw::from_bytes(&get_bytes(self.obj_id, self.key_id, TYPE_BYTES))
    }
}
