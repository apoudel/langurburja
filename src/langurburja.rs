// Copyright 2021 Aayush Poudel
// SPDX-License-Identifier: Apache-2.0
use std::collections::HashMap;

use wasmlib::*;

use crate::*;
use crate::structs::*;
use crate::contract::*;

const ENABLE_SELF_POST: bool = true;
const NANO_TIME_DIVIDER: i64 = 1_000_000_000;
const DEFAULT_PLAY_PERIOD: i32 = 60;
const MAX_SYMBOL_ID: i8 = 5;

pub fn func_force_reset(ctx: &ScFuncContext, f: &ForceResetContext) {
    f.state.bets().clear();
	f.state.round_status().set_value(0);
	f.events.stop();
}

pub fn func_init(ctx: &ScFuncContext, f: &InitContext) {
    if f.params.owner().exists() {
        f.state.owner().set_value(&f.params.owner().value());
        return;
    }
    f.state.owner().set_value(&ctx.contract_creator());

    let draw:Draw = Draw { dice1: 0, dice2: 1, dice3: 0, dice4: 2, dice5: 0, dice6: 3 };
    f.state.last_winning_draw().set_value( &draw );
}

pub fn func_pay_owner(ctx: &ScFuncContext, f: &PayOwnerContext) {
    let balances = ctx.balances();
    let iotaBalance: i64 = balances.balance( &ScColor::IOTA );

    ctx.log( &format!( "IOTA balance {}", iotaBalance )[..]);

	let amount = f.params.amount().value();

    if amount > 0 {
	    let transfers = ScTransfers::iotas( amount );
	    ctx.transfer_to_address( &f.state.owner().value().address(), transfers );
    }
}

pub fn func_pay_winners(ctx: &ScFuncContext, f: &PayWinnersContext) {
    // map if symbol_id -> symbol_count
    let mut symbol_map: HashMap<i8,i8> = HashMap::from([
        (0,0), // club  count
        (1,0), // flag  count
        (2,0), // spade count
        (3,0), // heart count
        (4,0), // brick count
        (5,0), // crown count
    ]);

    let mut winning_draw = Draw {
		dice1: 0,
        dice2: 0,
        dice3: 0,
        dice4: 0,
        dice5: 0,
        dice6: 0,
    };

    for i in 0..MAX_SYMBOL_ID + 1 {
	    let symbol_id: i8 = ctx.random( ( MAX_SYMBOL_ID + 1 ) as i64) as i8;
        *symbol_map.get_mut( &symbol_id ).unwrap() += 1;

        if i == 0 {
			winning_draw.dice1 = symbol_id;
		} else if i == 1 {
			winning_draw.dice2 = symbol_id;
		} else if i == 2 {
			winning_draw.dice3 = symbol_id;
		} else if i == 3 {
			winning_draw.dice4 = symbol_id;
		} else if i == 4 {
			winning_draw.dice5 = symbol_id;
		} else if i == 5 {
			winning_draw.dice6 = symbol_id;
		}
    }

	f.state.last_winning_draw().set_value( &winning_draw );

	f.events.winner( winning_draw.dice1, winning_draw.dice2, winning_draw.dice3, winning_draw.dice4, winning_draw.dice5, winning_draw.dice6 );

	let bets = f.state.bets();
	let nr_bets = bets.length();

	for i in 0..nr_bets {
		// Get the next winner.
		let bet = bets.get_bet(i).value();

        let symbol_count = symbol_map.get( &bet.symbol_id ).unwrap();

		if symbol_count >= &2 {
            let payout: i64 = bet.amount * (*symbol_count) as i64;

			let transfers = ScTransfers::iotas( payout );
			ctx.transfer_to_address( &bet.better.address(), transfers );

		    f.events.payout( &bet.better.address(), payout );
        }
	}

	bets.clear();

	// Set round status to 0, send out event to notify that the round has ended
	f.state.round_status().set_value(0);
	f.events.stop();
}

pub fn func_place_bet(ctx: &ScFuncContext, f: &PlaceBetContext) {
	let symbol_id = f.params.symbol_id().value();
	let bets = f.state.bets();

	for i in 0..bets.length() {
		let bet = bets.get_bet(i).value();

		if bet.better.address() == ctx.caller().address() && bet.symbol_id == symbol_id {
			ctx.panic("Bet already placed for this round");
		}
	}

	// Require that the number is a valid number to bet on, otherwise panic out.
	ctx.require( symbol_id >= 0 && symbol_id <= MAX_SYMBOL_ID, "invalid symbolId");

	// Create ScBalances proxy to the incoming balances for this request.
	// Note that ScBalances wraps an ScImmutableMap of token color/amount combinations
	// in a simpler to use interface.
	let incoming = ctx.incoming();

	// Retrieve the amount of plain iota tokens that are part of the incoming balance.
	let amount = incoming.balance( &ScColor::IOTA );

	// Require that there are actually some plain iotas there
	ctx.require( amount > 0, "empty bet");

	// Now we gather all information together into a single serializable struct
	// Note that we use the caller() method of the function context to determine
	// the agent id of the better. This is where a potential pay-out will be sent.
	let bet:Bet = Bet {
		better: ctx.caller(),
		amount: amount,
		symbol_id: symbol_id,
	};

	// Determine what the next bet number is by retrieving the length of the bets array.
	let bet_nr = bets.length();

	// Append the bet data to the bets array. The bet array will automatically take care
	// of serializing the bet struct into a bytes representation.
	bets.get_bet( bet_nr ).set_value( &bet );

	f.events.bet( &bet.better.address(), bet.amount, bet.symbol_id );

	// Was this the first bet of this round?
	if bet_nr == 0 {
		// Yes it was, query the state for the length of the playing period in seconds by
		// retrieving the playPeriod value from state storage
		let mut play_period = f.state.play_period().value();

		// if the play period is less than 10 seconds we override it with the default duration.
		// Note that this will also happen when the play period was not set yet because in that
		// case a zero value was returned.
		if play_period < 10 {
			play_period = DEFAULT_PLAY_PERIOD;
            f.state.play_period().set_value( play_period );
		}

		if ENABLE_SELF_POST {
			f.state.round_status().set_value(1);

			// timestamp is nanotime, divide by NanoTimeDivider to get seconds => common unix timestamp
			let timestamp = ( ctx.timestamp() / NANO_TIME_DIVIDER ) as i32;
			f.state.round_started_at().set_value( timestamp );

			f.events.start();

			let round_number = f.state.round_number();
			round_number.set_value( round_number.value() + 1);

			f.events.round( round_number.value());

			// And now for our next trick we post a delayed request to ourselves on the Tangle.
			// We are requesting to call the 'payWinners' function, but delay it for the playPeriod
			// amount of seconds. This will lock in the playing period, during which more bets can
			// be placed. Once the 'payWinners' function gets triggered by the ISCP it will gather
			// all bets up to that moment as the ones to consider for determining the winner.
			ScFuncs::pay_winners( ctx ).func.delay( play_period ).transfer_iotas(1).post();
		}
	}
}

pub fn func_play_period(ctx: &ScFuncContext, f: &PlayPeriodContext) {
	let play_period = f.params.play_period().value();
	ctx.require( play_period >= 10, "invalid play period");
	f.state.play_period().set_value( play_period );
}

pub fn func_set_owner(ctx: &ScFuncContext, f: &SetOwnerContext) {
    f.state.owner().set_value(&f.params.owner().value());
}

pub fn view_last_winning_draw(ctx: &ScViewContext, f: &LastWinningDrawContext) {
    let last_winning_draw = f.state.last_winning_draw().value();
	f.results.last_winning_draw().set_value( &last_winning_draw );
}

pub fn view_round_number(ctx: &ScViewContext, f: &RoundNumberContext) {
	let round_number = f.state.round_number().value();
	f.results.round_number().set_value( round_number );
}

pub fn view_round_started_at(ctx: &ScViewContext, f: &RoundStartedAtContext) {
	let round_started_at = f.state.round_started_at().value();
	f.results.round_started_at().set_value( round_started_at );
}

pub fn view_round_status(ctx: &ScViewContext, f: &RoundStatusContext) {
	let round_status = f.state.round_status().value();
	f.results.round_status().set_value( round_status );
}
